<?php

@session_start();

if(
    @$_SESSION['is_login'] ==null && 
    @$_SESSION['is_login'] == false
){
    header('location:login.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <title>Dashboard</title>

  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
</head>

<body>
  <!-- <?php 
    echo "ini halaman dashboardnya<br>".@$_SESSION['level']."Selamat Datang<br>".@$_SESSION['name'];
?> 
    
    <a href="Logout.php">
        <button type="button" class="btn btn-danger">Logout</button>
    </a> -->
    
    <div class= "navigation">
        
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">CariKerja.com</a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
           
        </nav>
        
    </div>
<?php
    switch (@$_SESSION['level']){
        case 'admin':
            include "Template/Navigation/nav_admin.php";
        break;
        case 'DataPerusahaan':
            include "Template/Navigation/nav_dataperusahaan.php";
        break;
        case 'user':
            include "Template/Navigation/nav_user.php";
        break;
    }
?>

<div class="content">
    
</div></div>
</body>

</html>
