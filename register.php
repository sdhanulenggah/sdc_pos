<?php
@session_start();

include "config/connect_db.php";

if (
    @$_SESSION['is_login'] != null &&
    @$_SESSION['is_login'] == true
) {
    header('location: dashboard.php');
}

$fullname = @$_POST['fullname'];
$email = @$_POST['email'];
$password = @$_POST['password'];
$confirmation_password = @$_POST['confirmation_password'];
$register = @$_POST['register'];

if ($register) {
    $query_check_email = "SELECT * FROM tb_user WHERE email = '$email' AND s_active = '1' AND is_trash = '0' ";
    $exec_query_check_email = mysql_query($query_check_email) or die(mysql_error());
    $result = mysql_fetch_array($exec_query_check_email);
    
    if (!$result) {

        if ($password == $confirmation_password) {
            $query_insert_data = "INSERT INTO tb_user ( name, email, password) VALUES ('$fullname', '$email', md5('$password'))";
            
            $exec_query_insert_data = mysql_query($query_insert_data) or die(mysql_error());

        
            if ($exec_query_insert_data) {
                echo "<script>alert('Berhasil Membuat Akun');</script>";

                @$_SESSION['is_login'] = true;
                @$_SESSION['level'] = 'user';
                header('location: dashboard.php');
            }
            } else {
                echo "<script>alert ('Password tidak sesuai');</script>";
            }
        } else {
            echo "<script>alert ('Email telah terdaftar');</script>";
        }
    }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <title>Register</title>

    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">

    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <form method="POST" action="">
                <h2>Create New Account</h2>

                <br>
                <label>Full Name</label>
                <input type="text" name="email" class="form-control" placeholder="Full Name" required>

                <br>
                <label>Email Address</label>
                <input type="email" name="email" class="form-control" placeholder="Email Address" required>

                <br>
                <label>Password</label>
                <input type="password" name="password" class="form-control" placeholder="Password" required>

                <br>
                <label>Confirmation Password</label>
                <input type="password" name="confirmation_password" class="form-control" placeholder="Confirmation Password" required>

                <br>
                <small>Sudah punya akun?<a href="login.php">Masuk disini</a></small>

                <br>
                <input type="submit" class="btn btn-primary" name="register" value="Register">
            </form>
        </div>
    </div>
</body>

</html>